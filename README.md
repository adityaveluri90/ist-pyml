######### This is the README file for ist-pyml project ##### 
######### Author: prasanna@insilitech.com              #####
############################################################


code:

	all the source go into this directory

data:

	the input data for supervised learning go into this directory

results:

	the results file will be stored in this directory


plots:

	the graphs and plots from the learning algorithms will be stored in this directory
