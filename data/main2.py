import csv
from pandas import DataFrame
from pandas import Series
from numpy import arange
from rdkit import Chem
from rdkit import DataStructs
from rdkit.Chem.Fingerprints import FingerprintMols
from scipy.cluster.vq import kmeans, vq

data = [] # This will contain our data
df1 = DataFrame(index=arange(185))
df2 = DataFrame(index=arange(185))
df3 = DataFrame(index=arange(185))
df4 = DataFrame(index=arange(185))
df5 = DataFrame(index=arange(185))
df6 = DataFrame(index=arange(185))
df7 = DataFrame(index=arange(185))
df8 = DataFrame(index=arange(185))
df9 = DataFrame(index=arange(185))
df10 = DataFrame(index=arange(185))
df11 = DataFrame(index=arange(185))
dfp = DataFrame(index=arange(184))

# Create a csv reader object to iterate through the file
reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')

for row in reader: 
		data.append( row[1] )
df2[1] = Series(data, index=df.index)
df3[1] = Series(data, index=df.index)
df4[1] = Series(data, index=df.index)
df5[1] = Series(data, index=df.index)
df6[1] = Series(data, index=df.index)
df7[1] = Series(data, index=df.index)
df8[1] = Series(data, index=df.index)
df9[1] = Series(data, index=df.index)
df10[1] = Series(data, index=df.index)
df11[1] = Series(data, index=df.index)
data = [] 

reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')

hrow = reader.next()
p = hrow.index('WD.polar')
q = hrow.index('FP1024')
r = hrow.index('ExtFP1024')
s = hrow.index('EStateFP79')
t = hrow.index('GraphFP1024')
u = hrow.index('MACCSFP166')
v = hrow.index('PubchemFP880')
m = hrow.index('SubFP307')
n = hrow.index('KRFP4860')
l = hrow.index('SubFPCFP307')
h = hrow.index('KRFPC4860')
z = hrow.index('SMILES')

for row in reader: 
		data.append( row[z] )

ms = [Chem.MolFromSmiles(x) for x in data]
fps = [FingerprintMols.FingerprintMol(y) for y  in ms]
data = []
for j  in arange(184):
	for i in arange(184):
		data.append( DataStructs.FingerprintSimilarity(fps[int(i)], fps[int(j)]))

	dfp[int(j)] = Series(data, index=dfp.index)
	data = []

subset = dfp[arange(184)]
tuples = [tuple(w) for w in subset.values]
d = array(tuples)
codebook, distortion = kmeans(d, 131)
code, dist = vq(d, codebook)
print code

reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')

for i in range(1, p+1):
	for row in reader: 
		data.append( row[i] )
	df1[int(i)] = Series(data, index=df.index)
	reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')
	data = []

df1.to_csv('trial1.csv', sep=', ', encoding='utf-8')

reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')

for i in range(p+1, q+1):
	for row in reader: 
		data.append( row[i] )
	df2[int(i)] = Series(data, index=df.index)
	reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')
	data = []

df2.to_csv('trial2.csv', sep=', ', encoding='utf-8')

reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')

for i in range(q+1, r+1):
	for row in reader: 
		data.append( row[i] )
	df3[int(i)] = Series(data, index=df.index)
	reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')
	data = []

df3.to_csv('trial3.csv', sep=', ', encoding='utf-8')

reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')

for i in range(r+1, s+1):
	for row in reader: 
		data.append( row[i] )
	df4[int(i)] = Series(data, index=df.index)
	reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')
	data = []

df4.to_csv('trial4.csv', sep=', ', encoding='utf-8')

reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')

for i in range(s+1, t+1):
	for row in reader: 
		data.append( row[i] )
	df5[int(i)] = Series(data, index=df.index)
	reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')
	data = []

df5.to_csv('trial5.csv', sep=', ', encoding='utf-8')

reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')

for i in range(t+1, u+1):
	for row in reader: 
		data.append( row[i] )
	df6[int(i)] = Series(data, index=df.index)
	reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')
	data = []

df6.to_csv('trial6.csv', sep=', ', encoding='utf-8')

reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')

for i in range(u+1, v+1):
	for row in reader: 
		data.append( row[i] )
	df7[int(i)] = Series(data, index=df.index)
	reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')
	data = []

df7.to_csv('trial7.csv', sep=', ', encoding='utf-8')

reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')

for i in range(v+1, m+1):
	for row in reader: 
		data.append( row[i] )
	df8[int(i)] = Series(data, index=df.index)
	reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')
	data = []

df8.to_csv('trial8.csv', sep=', ', encoding='utf-8')

reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')

for i in range(m+1, n+1):
	for row in reader: 
		data.append( row[i] )
	df9[int(i)] = Series(data, index=df.index)
	reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')
	data = []

df9.to_csv('trial9.csv', sep=', ', encoding='utf-8')

reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')

for i in range(n+1, l+1):
	for row in reader: 
		data.append( row[i] )
	df10[int(i)] = Series(data, index=df.index)
	reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')
	data = []

df10.to_csv('trial10.csv', sep=', ', encoding='utf-8')

reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')

for i in range(l+1, h+1):
	for row in reader: 
		data.append( row[i] )
	df11[int(i)] = Series(data, index=df.index)
	reader = csv.reader( open('T_HBT_I_Combined_data_184_Compounds.csv', 'rU'), delimiter='\t', dialect='excel')
	data = []

df11.to_csv('trial11.csv', sep=', ', encoding='utf-8')


